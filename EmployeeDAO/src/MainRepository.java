import java.sql.SQLException;
import java.util.List;

import dao.EmployeeDAO;
import dao.interfaces.IEmployeeDAO;
//importamos la entida de negocio: 
import model.EmployeeSQL;
// import repositories.H2Repository;
import repositories.PostgresReporitory;
import repositories.exception.EmployeeNotFoundException;
import repositories.exception.RepositoryException;
import repositories.interfaces.IRepository;

// import repositories.MysqlSQLRepository;
// import repositories.PostgreSQLReporitory;

//Esta clase representa un servicio:
public class MainRepository {

    public static void main(String[] args) {
        // Preguntamos si se paso algun paramatro:
        int searchById = 0;
        if (args.length > 0) {
            System.out.println("Setteando valor pasado como parametro: " + args[0]);
            searchById = Integer.parseInt(args[0]);
        } else {
            System.out.println("Setteando valor por defecto");
            searchById = 3;
        }

        // DAO (Data Access Object): Sirve para separar la logica del negocio del acceso
        // a datos.

        // Me traigo el dao y por detrar el dao una una implementacion concreta a traves
        // de una inyeccion
        // Inyeccion de dependencias:

        // Inversion de dependencias: No depender de modulos de bajo nivel.

        // Inversion de dependencias:
        // 1.- Los modulos de alto nivel no depende de los modulos de bajo nivel.
        // 2.- Dependes de al interface y no de los detalles.

        // Repositorios de prueba:
        IRepository repository = new PostgresReporitory();
        // IEmployeeRepository repository = new MysqlSQLRepository();
        // IEmployeeDAO repository = new MongoNoSQLRepository();
        // IEmployeeRepository repository = new H2Repository();

        IEmployeeDAO dao = new EmployeeDAO(repository);

        System.out.println("1.- [Se instancio el repositorio]: " + "\'" + repository.getClass() + "\'");
        System.out.println("2.- [inyectando dependencias]:  Se inyecto el repositorio: " + repository.getClass()
                + " al DAO: " + "\'" + dao.getClass() + "\'");
        System.out.println("3.- Ejecutando el programa... ");
        System.out.println("4.- Imprimiendo resultados... \n");

        EmployeeSQL employeeACreate = new EmployeeSQL("gamer3", "Javier", "gamer3@domain.com", "secret", false, true,
                1);

        try {
            // List<EmployeeSQL> list = dao.all();
            // EmployeeSQL employeeFinded = dao.getById(searchById);
            EmployeeSQL employeeCreated = dao.create(employeeACreate);
            System.out.println(employeeCreated.getEmployee_cve());
            // System.out.println("Employee finded!");
            // System.out.println("\tClave:" + employeeFinded.getEmployee_cve());
            // System.out.println("\tEmail:" + employeeFinded.getEmail());

            // list.forEach(emp -> {
            // System.out.println(emp.getEmployee_cve() + " " + emp.getEmail());
            // });
        } catch (EmployeeNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (RepositoryException ex) {
            System.out.println(ex.getMessage());
        }

        System.out.println("Se atrapa la excepcion y continua la ejecuccion del programa...");
    }
}