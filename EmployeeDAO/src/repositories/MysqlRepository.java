package repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import model.EmployeeSQL;
import repositories.interfaces.IRepository;

public class MysqlRepository implements IRepository {
    private Connection connection;

    public Connection createConnection() {
        try {
            String url = "jdbc:mysql://mysql.server.com/test?"; // + "user=minty&password=greatsqldb";
            return DriverManager.getConnection(url, "root", "secret");
        } catch (Exception e) {
            // TODO: handle exception
            return null;
        }
    }

    public void closeConnection() {
        try {
            this.connection.close();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    @Override
    public List<EmployeeSQL> all() {
        try {
            Connection connection = this.createConnection();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("Select * from employees");

            while (rs.next()) {
                // rs.getString("email");
                // System.out.println(rs.getString("email"));
                System.out.println(rs.getBoolean("active"));
            }
            return null;
        } catch (Exception e) {
            // TODO: handle exception
            return null;
        }
    }

    @Override
    public EmployeeSQL getById(int id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public EmployeeSQL create(EmployeeSQL newEmployee) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void update() {
        // TODO Auto-generated method stub

    }

    @Override
    public void delete() {
        // TODO Auto-generated method stub

    }

}
