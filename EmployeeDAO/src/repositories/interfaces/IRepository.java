package repositories.interfaces;
import java.util.List;

import model.EmployeeSQL;


// Con los SQL vas a poner otros dos metodos:
// Extiende la interfaz
public interface IRepository {
    public List<EmployeeSQL> all();

    public EmployeeSQL getById(int id);

    public EmployeeSQL create(EmployeeSQL newEmployee);

    public void update();

    public void delete();
}
