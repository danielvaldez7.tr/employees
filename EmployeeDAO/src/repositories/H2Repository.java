package repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.EmployeeSQL;
import repositories.interfaces.IRepository;

//Mas adelante vas a crear una funcion statica que encripte datos
public class H2Repository implements IRepository {
    private Connection connection;
    private String SQL_INSERT = "INSERT INTO employees(username,realname,email,password,active,isadmin,rol_cve) values (?,?,?,?,?,?,?)";

    public H2Repository() {
        // this.populateDatabase();
    }

    public Connection createConnection() {
        try {
            return DriverManager.getConnection("jdbc:h2:~/test", "h2", "secret");
        } catch (SQLException ex) {
            System.out.println("Exception: " + ex.getMessage());
            return null;
        }
    }

    public void closeConnection() {
        try {
            if (this.connection != null) {
                this.connection.close();
            }
        } catch (SQLException ex) {
            System.out.println("Exception: " + ex.getMessage());
        }
    }

    @Override
    public List<EmployeeSQL> all() {
        try {
            this.connection = createConnection();
            Statement st = this.connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM employees");

            List<EmployeeSQL> listEmployeeSQL = new ArrayList<EmployeeSQL>();

            while (rs.next()) {
                EmployeeSQL employeeSQLToAdd = new EmployeeSQL(rs.getInt("employee_cve"),
                        rs.getString("username"),
                        rs.getString("realname"),
                        rs.getString("email"),
                        rs.getString("password"),
                        rs.getBoolean("active"),
                        rs.getBoolean("isadmin"),
                        rs.getInt("rol_cve"));
                listEmployeeSQL.add(employeeSQLToAdd);
                employeeSQLToAdd = null;
            }
            st.close();
            rs.close();
            this.closeConnection();
            return listEmployeeSQL;
        } catch (Exception e) {
            // TODO: handle exception
            return null;
        } finally {
            this.closeConnection();
        }
    }

    public void populateDatabase() {
        List<EmployeeSQL> employees = new ArrayList<EmployeeSQL>();
        EmployeeSQL emp1 = new EmployeeSQL(1, "gamer1", "Joshep", "gamer1@domain.com", "secret", true, false, 1);
        EmployeeSQL emp2 = new EmployeeSQL(3, "gamer2", "Vanessa", "gamer2@domain.com", "secret", true, false, 1);
        EmployeeSQL emp3 = new EmployeeSQL(2, "gamer3", "Javier", "gamer3@domain.com", "secret", false, true, 1);
        employees.add(emp1);
        employees.add(emp2);
        employees.add(emp3);

        String sql = "CREATE TABLE employees" +
                "(employee_cve INTEGER NOT NULL" +
                "username VARCHAR DEFAULT NULL" +
                "realname VARCHAR DEFAULT NULL" +
                "email VARCHAR NOT NULL" +
                "password VARCHAR NOT NULL" +
                "active BIT NOT NULL DEFAULT '0'" +
                "isadmin BIT NOT NULL DEFAULT '0'" +
                "rol_cve INTEGER DEFAULT NULL );";

        // this.executeQuery(sql);
        employees.forEach(employee -> {
            this.create(employee);
        });
    }

    @Override
    public EmployeeSQL getById(int id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public EmployeeSQL create(EmployeeSQL newEmployee) {
        try {
            PreparedStatement ps = this.createConnection().prepareStatement(SQL_INSERT);
            ps.setString(1, newEmployee.getUsername());
            ps.setString(2, newEmployee.getRealname());
            ps.setString(3, newEmployee.getEmail());
            ps.setString(4, newEmployee.getPassword());
            ps.setBoolean(5, newEmployee.isActive());
            ps.setBoolean(6, newEmployee.isIsadmin());
            ps.setInt(7, newEmployee.getRol_cve());

            ps.execute();
            // Cuantos registros se insertaron:
            if (ps.getUpdateCount() >= 1) {
                System.out.println("Registro(s) insertado(s) con exito: " + ps.getUpdateCount());
            }

            return newEmployee;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;

        } catch (NullPointerException ex) {
            System.out.println(ex.getMessage());
            return null;
        } finally {
            this.closeConnection();
        }
    }

    @Override
    public void update() {
        // TODO Auto-generated method stub

    }

    @Override
    public void delete() {
        // TODO Auto-generated method stub

    }

    private void executeQuery(String sql) {
        System.out.println("Ejecutando consulta...");
        try {
            Statement st = this.createConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);

            String responseServer = "";
            while (rs.next()) {
                responseServer = rs.getString("CURRENT_USER");
            }
            System.out.println(responseServer);
        } catch (SQLException ex) {
            // TODO: handle exception

        } catch (NullPointerException ex) {
            System.out.println(ex.getMessage());
        } finally {
            this.createConnection();
        }
    }
}