package repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.EmployeeSQL;
import repositories.exception.EmployeeNotFoundException;
import repositories.exception.RepositoryException;
import repositories.interfaces.IRepository;

//Requerimientos:
//Por cada Driver necesito una implementacion concreta:
//Entonces aqui me conviene una facade o un dao
//El dao sirve para separar la logica del negocio del acceso a datos.

//Tengo una clase creadora de DAO's. Esta clase me creara una instancia:

public class PostgresReporitory implements IRepository {

    private String SQL_INSERT = "INSERT INTO employees( username,realname,email,password,active,isadmin,rol_cve) values (?,?,?,?,?,?,?)";
    private String SQL_SEARCHBYID = "SELECT employee_cve,username,realname,email,password,active,isadmin,rol_cve FROM employees WHERE employee_cve = ?";
    private Connection connection;

    public Connection createConnection() {
        try {
            String url = "jdbc:postgresql://localhost:5432/test";
            return DriverManager.getConnection(url, "postgres", "secret");
        } catch (SQLException ex) {
            throw new Error("SQL Error: " + ex);
        }
    }

    public void closeConnection() {
        try {
            if (this.connection != null) {
                this.connection.close();
            }
        } catch (SQLException ex) {
            throw new Error("SQL Error: " + ex);
        }
    }

    @Override
    public List<EmployeeSQL> all() {
        try {
            this.connection = createConnection();
            Statement st = this.connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM employees");

            List<EmployeeSQL> listEmployeeSQL = new ArrayList<EmployeeSQL>();
            EmployeeSQL userToAdd;

            while (rs.next()) {
                userToAdd = new EmployeeSQL(rs.getInt("employee_cve"),
                        rs.getString("username"),
                        rs.getString("realname"),
                        rs.getString("email"),
                        rs.getString("password"),
                        rs.getBoolean("active"),
                        rs.getBoolean("isadmin"),
                        rs.getInt("rol_cve"));
                listEmployeeSQL.add(userToAdd);
                userToAdd = null;
            }
            st.close();
            rs.close();
            return listEmployeeSQL;
        } catch (SQLException e) {
            // TODO: handle exception
            return null;
        } finally {
            this.closeConnection();
        }
    }

    @Override
    public EmployeeSQL getById(int searchid) {
        try {
            PreparedStatement ps = this.createConnection().prepareStatement(SQL_SEARCHBYID,
                    PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setInt(1, searchid);
            ResultSet rs = ps.executeQuery();

            EmployeeSQL employeeFinded = new EmployeeSQL();

            while (rs.next()) {
                employeeFinded.setEmployee_cve(rs.getInt("employee_cve"));
                employeeFinded.setUsername(rs.getString("username"));
                employeeFinded.setRealname(rs.getString("realname"));
                employeeFinded.setEmail(rs.getString("email"));
                employeeFinded.setPassword(rs.getString("password"));
                employeeFinded.setActive(rs.getBoolean("active"));
                employeeFinded.setIsadmin(rs.getBoolean("isadmin"));
                employeeFinded.setRol_cve(rs.getInt("rol_cve"));
            }

            ps.close();
            rs.close();
            if (employeeFinded.getEmployee_cve() == 0) {
                throw new NullPointerException();
            }

            return employeeFinded;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        } catch (NullPointerException ex) {
            throw new EmployeeNotFoundException("Employee with id: " + searchid + " Not Found!");
        } finally {
            this.closeConnection();
        }
    }

    @Override
    public EmployeeSQL create(EmployeeSQL newEmployee) {
        try {
            PreparedStatement ps = this.createConnection().prepareStatement(SQL_INSERT,
                    PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, newEmployee.getUsername());
            ps.setString(2, newEmployee.getRealname());
            ps.setString(3, newEmployee.getEmail());
            ps.setString(4, newEmployee.getPassword());
            ps.setBoolean(5, newEmployee.isActive());
            ps.setBoolean(6, newEmployee.isIsadmin());
            ps.setInt(7, newEmployee.getRol_cve());

            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();

            while (rs.next()) {
                newEmployee.setEmployee_cve(rs.getInt(1));
            }

            // Cuantos registros se insertaron:
            if (ps.getUpdateCount() >= 1) {
                System.out.println("Registro(s) insertado(s) con exito: " + ps.getUpdateCount());
            }
            return newEmployee;
        } catch (SQLException ex) {
            throw new RepositoryException("SQL Error: " + ex);
        } catch (NullPointerException ex) {
            throw new RepositoryException("Null Pointer Exception: " + ex);
        } finally {
            this.closeConnection();
        }
    }

    @Override
    public void update() {
        // TODO Auto-generated method stub

    }

    @Override
    public void delete() {
        // TODO Auto-generated method stub

    }

    public static void main(String[] args) {
        System.out.println("Hola mundo ");
    }

    public void populateDatabase() {
        List<EmployeeSQL> employees = new ArrayList<EmployeeSQL>();
        EmployeeSQL emp1 = new EmployeeSQL(1, "gamer1", "Joshep", "gamer1@domain.com", "secret", true, false, 1);
        EmployeeSQL emp2 = new EmployeeSQL(3, "gamer2", "Vanessa", "gamer2@domain.com", "secret", true, false, 1);
        EmployeeSQL emp3 = new EmployeeSQL(2, "gamer3", "Javier", "gamer3@domain.com", "secret", false, true, 1);
        employees.add(emp1);
        employees.add(emp2);
        employees.add(emp3);

        String sql = "CREATE TABLE employees" +
                "(employee_cve INTEGER NOT NULL" +
                "username VARCHAR DEFAULT NULL" +
                "realname VARCHAR DEFAULT NULL" +
                "email VARCHAR NOT NULL" +
                "password VARCHAR NOT NULL" +
                "active BIT NOT NULL DEFAULT '0'" +
                "isadmin BIT NOT NULL DEFAULT '0'" +
                "rol_cve INTEGER DEFAULT NULL );";

        // this.executeQuery(sql);
        employees.forEach(employee -> {
            this.create(employee);
        });
    }
}