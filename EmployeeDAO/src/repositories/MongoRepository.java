package repositories;

import java.util.List;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.MongoClientSettings.getDefaultCodecRegistry;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import org.bson.codecs.configuration.CodecConfigurationException;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.ClassModel;
import org.bson.codecs.pojo.ClassModelBuilder;
import org.bson.codecs.pojo.PojoCodecProvider;
import com.mongodb.MongoSocketOpenException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import dao.interfaces.IEmployeeDAO;
import model.EmployeeSQL;

public class MongoRepository implements IEmployeeDAO {
    MongoClient mongoClient;

    private MongoClient createConnection() {
        try {
            String uri = "mongodb://mongoadmin:mongoadmin@mongo.server.com:27017/test";
            return MongoClients.create(uri);
        } catch (MongoSocketOpenException ex) {
            System.out.println(ex.getCode());
            System.out.println(ex.getMessage());
            // Lanzar una excepcion
            return null;
        }
    }

    private void closeConnection() {
        if (mongoClient != null) {
            mongoClient.close();
        }
    }

    @Override
    public List<EmployeeSQL> all() {

        try {
            // getDefaultCodecRegistry(): Esta propiedad la debo de hacer custom
            // PropertyCodecRegistry

            ClassModelBuilder<EmployeeSQL> employeeModel = ClassModel.builder(EmployeeSQL.class);

            CodecProvider pojoCodecProvider = PojoCodecProvider
                    .builder()
                    .register(employeeModel.build())
                    .build();

            CodecRegistry pojoCodecRegistry = fromRegistries(getDefaultCodecRegistry(),
                    fromProviders(pojoCodecProvider));
            mongoClient = this.createConnection();

            MongoDatabase database = mongoClient.getDatabase("test").withCodecRegistry(pojoCodecRegistry);
            MongoCollection<EmployeeSQL> collection = database.getCollection("employees", EmployeeSQL.class);
            EmployeeSQL emp = collection.find(eq("email", "kaoz@domain.com")).first();
            System.out.println(emp.getEmail());

            return null;
        } catch (CodecConfigurationException ex) {
            System.out.println(ex.getMessage());
            // Lanzar una excepcion
            return null;
        } finally {
            this.closeConnection();
        }
    }

    @Override
    public void update() {
        // TODO Auto-generated method stub

    }

    @Override
    public void delete() {
        // TODO Auto-generated method stub

    }

    @Override
    public EmployeeSQL getById(int id) {
        Integer parseId = new Integer(id);

        ClassModelBuilder<EmployeeSQL> employeeModel = ClassModel.builder(EmployeeSQL.class);

        CodecProvider pojoCodecProvider = PojoCodecProvider
                .builder()
                .register(employeeModel.build())
                .build();

        CodecRegistry pojoCodecRegistry = fromRegistries(getDefaultCodecRegistry(),
                fromProviders(pojoCodecProvider));
        mongoClient = this.createConnection();

        MongoDatabase database = mongoClient.getDatabase("test").withCodecRegistry(pojoCodecRegistry);
        MongoCollection<EmployeeSQL> collection = database.getCollection("employees", EmployeeSQL.class);
        // Lanzar una exception si nos regresa nulo:
        return collection.find(eq("employee_cve", parseId.toString())).first();
    }

    @Override
    public EmployeeSQL create() {
        // TODO Auto-generated method stub
        return null;
    }

}
