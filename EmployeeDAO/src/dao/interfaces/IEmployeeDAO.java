package dao.interfaces;

import java.sql.SQLException;
import java.util.List;

import model.EmployeeSQL;

public interface IEmployeeDAO {

    public List<EmployeeSQL> all();

    public EmployeeSQL getById(int id);

    public EmployeeSQL create(EmployeeSQL newEmployeeSQL);

    public void update() throws SQLException;

    public void delete() throws SQLException;
}
