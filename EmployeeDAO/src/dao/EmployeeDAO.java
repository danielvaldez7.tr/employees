package dao;

import java.util.List;

import dao.interfaces.IEmployeeDAO;
import model.EmployeeSQL;
import repositories.interfaces.IRepository;

public class EmployeeDAO implements IEmployeeDAO {

    private IRepository repository;

    // Inyeccion de dependencias:
    public EmployeeDAO(IRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<EmployeeSQL> all() {
        return repository.all();
    }

    @Override
    public EmployeeSQL create(EmployeeSQL newEmployeeSQL) {
        return repository.create(newEmployeeSQL);
    }

    @Override
    public EmployeeSQL getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void update() {
        // TODO Auto-generated method stub

    }

    @Override
    public void delete() {
        // TODO Auto-generated method stub

    }

}
