package model;

import org.bson.BsonType;
import org.bson.codecs.pojo.annotations.BsonProperty;

public class EmployeeSQL {
	@BsonProperty("_id")
	private int id;

	private int employee_cve;
	private String username = null;
	private String realname = null;
	private String email;
	private String password;
	private boolean active;
	private boolean isadmin;
	private int rol_cve; // Campo opcional

	public EmployeeSQL() {
	}

	public EmployeeSQL(String username, String realname, String email, String password,
			boolean active, boolean isadmin, int rol_cve) {
		this.username = username;
		this.realname = realname;
		this.email = email;
		this.password = password;
		this.active = active;
		this.isadmin = isadmin;
		this.rol_cve = rol_cve;
	}

	public EmployeeSQL(int employee_cve, String username, String realname, String email, String password,
			boolean active, boolean isadmin, int rol_cve) {
		this.employee_cve = employee_cve;
		this.username = username;
		this.realname = realname;
		this.email = email;
		this.password = password;
		this.active = active;
		this.isadmin = isadmin;
		this.rol_cve = rol_cve;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEmployee_cve() {
		return employee_cve;
	}

	public void setEmployee_cve(int employee_cve) {
		this.employee_cve = employee_cve;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isIsadmin() {
		return isadmin;
	}

	public void setIsadmin(boolean isadmin) {
		this.isadmin = isadmin;
	}

	public int getRol_cve() {
		return rol_cve;
	}

	public void setRol_cve(int rol_cve) {
		this.rol_cve = rol_cve;
	}

}
