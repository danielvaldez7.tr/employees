#!/bin/zsh
#Este script es una utileria para administrar el modulo repository:

if [[ $USER = "daniel" ]]; then
	PGPASSFILE=""
	echo "importando archivo "
	#import acceso a datos:
	file="/Users/daniel/files/.pgpass"
	export PGPASSFILE=$file
fi

ROOT_MODULE="/Users/daniel/Documents/testing_java/EmployeeDAO"

libray_path="./library"
source_code_path=$ROOT_MODULE/src
bin_path=$ROOT_MODULE/target

#Dependencies
library1="$libray_path/lombok-1.18.24.jar"

library2="$libray_path/postgresql-42.5.0.jar"
library3="$libray_path/mysql-connector-j-8.0.31.jar"


library4="$libray_path/mongodb-driver-3.12.11.jar"
library5="$libray_path/mongodb-driver-core-3.12.11.jar"
library6="$libray_path/lf4j-api-1.6.1.jar"
library7="$libray_path/bson-3.12.11.jar"

library8="$libray_path/jnr-unixsocket-0.18.jar"
library9="$libray_path/mongodb-crypt-1.0.1.jar"
library10="$libray_path/netty-buffer-4.1.17.Final.jar"
library11="$libray_path/netty-handler-4.1.17.Final.jar"
library9="$libray_path/netty-transport-4.1.17.Final.jar"
library10="$libray_path/snappy-java-1.1.4-javadoc.jar"
library11="$libray_path/zstd-jni-1.3.8-3.jar"
library12="$libray_path/org.osgi.core-4.3.0.jar"

library13="$libray_path/h2-2.1.214.jar"

CLASSPATH=""
CLASSPATH=$source_code_path:$bin_path
CLASSPATH=$CLASSPATH:$library1:$library2:$library3

CLASSPATH=$CLASSPATH:$library4:$library5:$library6:$library7
# CLASSPATH=$CLASSPATH:$library8:$library9:$library10:$library11:$library12
CLASSPATH=$CLASSPATH:$library13

export CLASSPATH=$CLASSPATH

#Main clase:
main_class=MainRepository

# $source_code_path/"$MainRepository.java"
#$bin_path/MainRepository
clear
javac -d $bin_path $source_code_path/"$main_class.java" -proc:none && java MainRepository $1
