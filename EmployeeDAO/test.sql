

--
-- Name: employees; Type: TABLE; Schema: public; Owner: postgres
--

/*CREATE TABLE employees (
    employee_cve integer NOT NULL,
    username character varying(50),
    realname character varying(50),
    email character varying(50) NOT NULL,
    password character varying(150) NOT NULL,
    active boolean DEFAULT false NOT NULL,
    isadmin boolean DEFAULT false NOT NULL,
    rol_cve integer
);
*/


--
-- Data for Name: employees; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO employees (employee_cve, username, realname, email, PASSWORD, active, isadmin, rol_cve)
    VALUES (1, 'kaoz', 'daniel', 'kaoz@domain.com', 'secret', 1, 0, 1),
    (2, 'gl3nttl3m4n', 'daniel', 'gleteman@domain.com', 'secret', 1, 0, 1),
    (3, 'pir4t3', 'daniel', 'thepir4t3@domain.com', 'secret', 0, 0, 1);



--
-- Name: employees fk_rolcve_roles; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--



--
-- PostgreSQL database dump complete
--

