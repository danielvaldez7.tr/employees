import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Order {
    Customer customer;
    Address billingAddress;

    public Order() {
    }

    public Order(Customer customer, Address billingAddress) {
        this.customer = customer;
        this.billingAddress = billingAddress;
    }
}