import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Name {
    String firstName;
    String lastName;
}
