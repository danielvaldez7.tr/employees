import lombok.Setter;
import lombok.Getter;
import org.modelmapper.ModelMapper;

@Getter
@Setter
public class OrderDTO {
    public OrderDTO() {
        System.out.println("Construyendo un DTO");
    }

    String customerFirstName;
    String customerLastName;
    String billingStreet;
    String billingCity;
}
