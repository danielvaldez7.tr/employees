#!/bin/zsh
# Programa de utilidades para compilar y ejecutar programas escritos en java.

# src/: Contiene el codigo fuente de la aplicacion
# target/: Contiene los ejecutables y binarios de la aplicacion
# library: Contiene las dependencias y librerias de terceros

# Variable del Sistema
# PATH: Variable de entorno principal del sistema. En esta ruta esta definidos los ejecutables del sistema operativo.
# JAVA_HOME: Variable de entorno que indica al sistema operativo la ruta hacia el home de la version de JDK a utilizar.
# CLASSPATH: Variable de entorno que indica a la JVM donde buscar los binarios clases y ejecutables de la aplicacion.

ROOT_PROJECT=/home/$USER/Docucuments/testing_java
# WORKSPACE=ROOT_PROJECT

init_project() {
    echo "[log]-[time: 2022/11/05] Creando la estructura base del proyecto"
    base_directories=(target src library)
    # Crea los directorios
    create_structure_project() {
        for base_dir in ${base_directories[@]}; do
            if [[ ! -d $base_dir ]]; then
                echo "[log]-[time: 2022/11/05] Creando directorio: '$base_dir'" # A futuro Mandarlo a un log con fecha y hora:
                mkdir $base_dir
            else
                echo "[log]-[time: 2022/11/05] Saltando... el directorio: '$base_dir' ya existe"
            fi
        done
    }

    create_structure_project
}

# Agrega librerias al classpath:
add_libraries_to_classpath() {
    clean_classpath
    libraries=""

    echo "Agregando librerias..."
    for lib_name in $(ls ./library/); do
        path_library="./library/$lib_name:"
        libraries="$libraries$path_library"
    done

    export CLASSPATH="$CLASSPATH:$libraries"
}

add_library_classpath() {
    path_add_library=$1
    #Verifica que la variable no este vacia
    if [[ -z $path_add_library ]]; then
        echo "[time: error] Tienes que agregar la ruta de la libreria: 'library/library-example.jar'"
        # termina el programa:
    else
        library_to_add=$(basename $path_add_library)
    fi

    isAdded="false"
    for d in $(echo $CLASSPATH); do
        # Esta esta libreria en tu collecion?
        if [[ $d == $library_to_add ]]; then
            echo "coincidencia encontrada! Ya esta agregada la libreria"
            isAdded="true"
            break
        fi
    done

    if [[ $isAdded == "false" ]]; then
        echo "Agregando la libreria: '$library_to_add' al CLASSPATH"
        export CLASSPATH=$CLASSPATH:$library_to_add
    fi
}

print_classpath() {
    if [[ -z $CLASSPATH ]]; then
        echo "No esta setteada la variable: CLASSPATH"
    fi
    
    #Depende de este binario: command node
    echo 'console.log({classpath: process.argv[2].split(":")});' | node - "$CLASSPATH"
}

# Configura el espacio de trabajo
set_environment() {
    clean_classpath
    add_libraries_to_classpath

    dir_source_code=./src/com/empresa/project

    PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin"
    export PATH=$PATH

    export JAVA_HOME="/Library/Java/JavaVirtualMachines/jdk-11.0.14.jdk/Contents/Home"
    #export JAVA_HOME="/Users/daniel/Library/Java/JavaVirtualMachines/corretto-1.8.0_342/Contents/Home"
    #export CLASSPATH="$CLASSPATH./src/:./target/"

    export CLASSPATH="$CLASSPATH:$dir_source_code:./target/"
    export NODE_HOME="/Users/daniel/.nvm/versions/node/v16.15.1"
    export MYSQL_BIN="/usr/local/opt/mysql-client"

    env_variables=($JAVA_HOME/bin $NODE_HOME/bin $MYSQL_BIN/bin)
    added_env_var=""
    for env in ${env_variables[@]}; do
        added_env_var="$added_env_var$env:"
    done
    # echo $PATH:$added_env_var
    # echo $added_env_var
    export PATH=$PATH:$added_env_var
}

# Limpia la variable de entorno
clean_classpath() {
    export CLASSPATH=""
}

# Compila el codigo fuente en bytecode
compile() {
    # source_code_dir=./src/com/empresa/project
    # source_code_dir="./src/com/empresa/project/api"
    workspace="./src/com/empresa/project"
    main_class="Principal.java" # Agrega un default

    echo "Compilando..."
    # javac -d target src/Main.java -Xlint -proc:none
    javac -d ./target "$workspace/$main_class" -Xlint -proc:none
}

# Executa el binario que contiene el metodo principal en la JVM
execute() {
    main_class="Principal" # Agrega un default

    echo "Ejecutando..."
    java "${main_class}"
}

# Lista los binarios compilados por java
list_class() {
    find ./target -type f -name "*.class"
}

# Elimina los archivo binarios generados por el compilador de java
clean_binaries() {
    find ./target -type f -name "*.class" -delete
}

run_app() {
    if [[ -z $CLASSPATH ]]; then
        set_environment
    fi
    clear
    compile && execute
    sleep 1
}